/*
  Décommenter enum tempo car toutes les tempos sont sur 2 secondes

  Fonctionne 3 févier 2019
  Avant modifiction
  Supression de l'inversion de switch mise en commentaire
  Changement des pins moteur moteur 1 en 42 moteur 2 en 43
  Inversion goute à goute et gaz
  Relais 7 et 8 dispo

*/
#include <RBD_Timer.h>  // https://github.com/alextaujenis/RBD_Timer
#include <RBD_Button.h> // https://github.com/alextaujenis/RBD_Button
#include <RBD_Light.h> // https://github.com/alextaujenis/RBD_Light
/*
  Tour du monde en 80 secondes
  Cedric Cordelette 06.70.89.87.23
  Janvier 2019
*/

/*
   Déclaration des  leds
*/
RBD::Light LED0_Info        (13);
RBD::Light LED1_FRANCE      (4);
RBD::Light LED2_AFRIQUE     (5);
RBD::Light LED3_ANT_JAR     (6);
RBD::Light LED4_ANT_COUR    (7);
RBD::Light LED5_MUR_MEXIQUE (8);
RBD::Light LED6_USA         (9);
RBD::Light LED7_ART         (10);
RBD::Light lED8_CHINE       (11);
RBD::Light LED9_ILE_REVE    (12);
//Tableau de led
RBD::Light LedTab [] = {LED0_Info, LED1_FRANCE, LED2_AFRIQUE, LED3_ANT_JAR, LED4_ANT_COUR, LED5_MUR_MEXIQUE, LED6_USA, LED7_ART, lED8_CHINE, LED9_ILE_REVE};

/*
   Déclaration des relais et moteurs
*/
RBD::Light Quartz       (38);
RBD::Light Tajine       (39);
RBD::Light GouteGoute   (40);
RBD::Light Gaz          (41);
RBD::Light Moteur_1     (42);
RBD::Light Moteur_2     (43);
RBD::Light Relais_7     (44);   //Relais 7 dispo
RBD::Light Relais_8     (45);   //Relais 8 dispo
//Tableau des sorties
RBD::Light  TabOut [] = {Quartz, Tajine, Gaz, GouteGoute, Moteur_1, Moteur_2, Relais_7, Relais_8};

/*
   Déclaration des capteurs Map et des boutons pupitre
*/
RBD::Button BP_Cylcle     (22);         // monde pullup Cycle
RBD::Button BP_Reset      (23);         // mode pullup  repart à l'étape 0
RBD::Button BP_Stop      (24);         // monde pullup mise en pause des actions  reset
RBD::Button S1_FRANCE     (26, false);  // mode down
RBD::Button S2_AFRIQUE    (27, false);  // mode down
RBD::Button S3_ANT_JAR    (28, false);  // mode down
RBD::Button S4_ANTAR      (29, false);  // mode down
RBD::Button S5_ANTAR_COUR (30, false);  // mode down
RBD::Button S6_AM_SUD     (31, false);  // mode down
RBD::Button S7_MEXIQUE    (32, false);  // mode down
RBD::Button S8_USA        (33, false);  // mode down
RBD::Button S9_ART        (34, false);  // mode down
RBD::Button S10_CHINE     (35, false);  // mode down
RBD::Button S11_ILE_REVE  (36, false);  // mode down

// Déclaration des switch en Pullup ****** à supprimer
//  RBD::Button S1_FRANCE     (26);
//  RBD::Button S2_AFRIQUE    (27);
//  RBD::Button S3_ANT_JAR    (28);
//  RBD::Button S4_ANTAR      (29);
//  RBD::Button S5_ANTAR_COUR (30);
//  RBD::Button S6_AM_SUD     (31);
//  RBD::Button S7_MEXIQUE    (32);
//  RBD::Button S8_USA        (33);
//  RBD::Button S9_ART        (34);
//  RBD::Button S10_CHINE     (35);
//  RBD::Button S11_ILE_REVE  (36);

//Tableau des Bouton
RBD::Button TabBP [] = {BP_Cylcle, S1_FRANCE, S2_AFRIQUE, S3_ANT_JAR, S4_ANTAR, S5_ANTAR_COUR, S6_AM_SUD, S7_MEXIQUE, S8_USA, S9_ART, S10_CHINE, S11_ILE_REVE, BP_Reset, BP_Stop};

/*
  Déclaration d'une tempo
*/
RBD::Timer MyTimer;
/*
  Variable Globale
*/
enum timeEnum {timerEtape_1 = 8000, timerEtape_3 = 2000, timerEtape_6 = 2000, timerEtape_14 = 10000}; //temps en milliseconde des différentes tempo
//enum timeEnum {timerEtape_1 = 1000, timerEtape_3 = 2000, timerEtape_6 = 2000, timerEtape_14 = 2000}; //temps en milliseconde des différentes tempo
enum etape {ETAPE_0, ETAPE_1, ETAPE_2, ETAPE_3, ETAPE_4, ETAPE_5, ETAPE_6, ETAPE_7, ETAPE_8, ETAPE_9, ETAPE_10, ETAPE_11, ETAPE_12, ETAPE_13, ETAPE_14, ETAPE_15, ETAPE_16, ArretUrgence};
int psositionCycle;

/*  +++++++++++++++++++++++++++++++++++++
          voidDéclaration des fonctions
    +++++++++++++++++++++++++++++++++++++
*/
void arretUrgence();
void pause();
void allLedOn ();
void allLedOff();
//void allOuton();
void allOutOff();
void lectureOut();
void etatOut();
/*  +++++++++++++++++++++++++++++++++++++
          void setup()
    +++++++++++++++++++++++++++++++++++++
*/
void setup() {
  //Serial.begin(9600);

  //  // Configuration de la lecture des bouton sur un niveau 1 (5V)
  //  S1_FRANCE.invertReading();
  //  S2_AFRIQUE.invertReading();
  //  S3_ANT_JAR.invertReading();
  //  S4_ANTAR.invertReading();
  //  S5_ANTAR_COUR.invertReading();
  //  S6_AM_SUD.invertReading();
  //  S7_MEXIQUE.invertReading();
  //  S8_USA.invertReading();
  //  S9_ART.invertReading();
  //  S10_CHINE.invertReading();
  //  S11_ILE_REVE.invertReading();

  //Lecture des boutton pour init
  lectureOut();

  //Allunmage de toutes les led pendant 1,5 seconde
  allLedOn();
  delay(1500);
  allLedOff();

  psositionCycle = ETAPE_15;
}

/*  +++++++++++++++++++++++++++++++++++++
          void loop()
    +++++++++++++++++++++++++++++++++++++
*/
void loop() {
  if (BP_Reset.onPressed()) {
    //Serial.println("Boutton Reset onPressed");
//    Moteur_1.off();
//    Moteur_2.off();
//    psositionCycle = ETAPE_15;
  } else if (BP_Stop.onPressed()) {
    //Serial.println("BP stop BP_Stop.onPressed()");
    Moteur_1.off();
    Moteur_2.off();
    psositionCycle = ETAPE_15;
    pause();
  }

  switch (psositionCycle) {

    case ETAPE_0:
      lectureOut();
      delay(500);
      lectureOut();
      //Serial.println("ETAPE_0");
      LED1_FRANCE.on();
      psositionCycle ++;
      //Serial.println("ETAPE_1");
      break;

    case ETAPE_1:
      if (BP_Cylcle.onPressed()) {
        LED1_FRANCE.off();
        LED2_AFRIQUE.on();
        MyTimer.setTimeout(timerEtape_1);
        MyTimer.restart();
      } else if (MyTimer.isExpired() && LED2_AFRIQUE.isOn()) {
        Moteur_1.on();
        lectureOut();
        psositionCycle ++;
        //Serial.println("ETAPE_2");
      }
      break;

    case ETAPE_2:

      if (S2_AFRIQUE.onPressed()) {
        Moteur_1.off();
        Tajine.on();
        lectureOut();
        psositionCycle++;
        //Serial.println("ETAPE_3");
      }
      break;

    case ETAPE_3:
      if (BP_Cylcle.onPressed()) {
        Moteur_1.on();
        LED2_AFRIQUE.off();
        LED3_ANT_JAR.on();
        MyTimer.setTimeout(timerEtape_3);
        MyTimer.restart();
      }  else if (LED3_ANT_JAR.isOn()  && MyTimer.isExpired()) {
        Tajine.off();
        lectureOut();
        psositionCycle ++;
        //Serial.println("ETAPE_4");
      }
      break;

    case ETAPE_4:
      if (S3_ANT_JAR.onPressed()) {
        LED3_ANT_JAR.off();
        LED4_ANT_COUR.on();
        psositionCycle ++;
        lectureOut();
        //Serial.println("ETAPE_5");
      }
      break;

    case ETAPE_5:
      if (S4_ANTAR.onPressed()) {
        Moteur_1.off();
        lectureOut();
        psositionCycle ++;
        //Serial.println("ETAPE_6");
      }
      break;

    case ETAPE_6:
      if (BP_Cylcle.onPressed()) {
        Moteur_2.on();
        MyTimer.setTimeout(timerEtape_6);
        MyTimer.restart();
      } else if (MyTimer.isExpired() && Moteur_2.isOn()) {
        Gaz.blink(2000, 100, 1);
        Gaz.update();
        lectureOut();
        psositionCycle ++;
        //Serial.println("ETAPE_7");
      }

    case ETAPE_7:
      if (Gaz.isOn()) {
        //Serial.println("Gas is On");
        Gaz.update();
      }
      if (S6_AM_SUD.onPressed()) {
        //Serial.println("S6_AM_SUD.onPressed()");
        LED4_ANT_COUR.off();
        LED6_USA.on();
        lectureOut();
        psositionCycle ++;
        //Serial.println("ETAPE_8");
      }
      break;

    case ETAPE_8:
      if (S7_MEXIQUE.onPressed()) {
        //Serial.println("S7 onPressed");
        Moteur_2.off();
        LED5_MUR_MEXIQUE.on();
        Quartz.on();
        lectureOut();
        psositionCycle ++;
        //Serial.println("ETAPE_9");
      }
      break;

    case ETAPE_9:
      if (BP_Cylcle.onPressed()) {
        //Serial.println("BP_Cylcle.onPressed()");
        Moteur_2.on();
        Quartz.off();
        LED5_MUR_MEXIQUE.off();
        lectureOut();
        psositionCycle ++;
        //Serial.println("ETAPE_10");
      }
      break;

    case ETAPE_10:
      if (S8_USA.onPressed()) {
        //Serial.println("S8_USA.onPressed()");
        LED6_USA.off();
        LED7_ART.on();
        lectureOut();
        lectureOut();
        psositionCycle ++;
        //Serial.println("ETAPE_11");
      }
      break;

    case ETAPE_11:
      if (S9_ART.onPressed()) {
        GouteGoute.on();
        LED7_ART.off();
        lED8_CHINE.on();
        lectureOut();
        psositionCycle ++;
        //Serial.println("ETAPE_12");
      }
      break;

    case ETAPE_12:
      if (S10_CHINE.onPressed()) {
        Moteur_2.off();
        lectureOut();
        psositionCycle ++;
        //Serial.println("ETAPE_13");
      }
      break;

    case ETAPE_13:
      if (BP_Cylcle.onPressed()) {
        Moteur_2.on();
        lED8_CHINE.off();
        LED9_ILE_REVE.on();
        lectureOut();
        psositionCycle ++;
        //Serial.println("ETAPE_14");
      }
      break;

    case ETAPE_14:
      if (S11_ILE_REVE.onPressed()) {
        Moteur_2.off();
        GouteGoute.off();
        MyTimer.setTimeout(timerEtape_14);
        MyTimer.restart();
      } else if (Moteur_2.isOff() && MyTimer.isExpired()) {
        LED9_ILE_REVE.off();
        lectureOut();
        psositionCycle ++;
        //Serial.println("ETAPE_15");
      }
      break;

    case ETAPE_15:
      if (BP_Cylcle.onPressed()) {
        Moteur_1.on();
        Moteur_2.on();
        psositionCycle ++;
        //Serial.println("ETAPE_16");
      }
      break;

    case ETAPE_16:
      if (S1_FRANCE.onPressed()) {
        Moteur_1.off();
        //LED1_FRANCE.on();
      }
      if (S5_ANTAR_COUR.onPressed()) {
        Moteur_2.off();
      }
      if (Moteur_1.isOff() && Moteur_2.isOff()) {
        lectureOut();
        psositionCycle = ETAPE_0;
      }
      lectureOut();
      break;

    case ArretUrgence:
      //Serial.println("Etape 17");
      if (BP_Cylcle.onPressed()) {
        psositionCycle = ETAPE_6;
      }
      break;

    default:
      // statements
      break;
  }
}


