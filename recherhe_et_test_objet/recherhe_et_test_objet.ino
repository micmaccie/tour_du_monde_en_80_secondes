

// Arduino RBD Button Library Example v2.1.1 - Debounce a button with events. INPUT_PULLUP Disabled Warning: this example requires resistors in your circuit.
// https://github.com/alextaujenis/RBD_Button
// Copyright 2016 Alex Taujenis
// MIT License

#include <RBD_Timer.h>  // https://github.com/alextaujenis/RBD_Timer
#include <RBD_Button.h> // https://github.com/alextaujenis/RBD_Button
#include <RBD_Light.h> // https://github.com/alextaujenis/RBD_Light

// INPUT_PULLUP Disabled Warning: this example requires resistors in your circuit

RBD::Button button(22); // pin number, input_pullup flag
RBD::Light light(4);
RBD::Light light2(5);
RBD::Light light3(6);
RBD::Light light4(7);


RBD::Timer timer;


void setup() {
  Serial.begin(9600);
  button.setDebounceTimeout(20);
  button.onReleased();
  light.off();
  button.onPressed();

}

void loop() {
  //light2.update();
  if (button.onPressed()) {
    Serial.println("Button Pressed");

    if (light.isOff()) {
      light.on();
    } else {
      light.off();
    }
    light2.blink(3000, 100, 1);
    light2.update();
    light3.on();
    timer.setTimeout(10000);
    timer.restart();
    //light4.on();// pour tester si on peut allumer une led et ensuite lui faire un bink
  }

  if (button.onReleased()) {
    Serial.println("Button Released");
    light4.blink(2000, 10, 1);
    light4.update();

  }

  if (light2.isOn()) {
    light2.update();//remplace le light2.update du début void loop()
  }
  if (timer.isExpired() && light3.isOn()) {
    light3.off();
    Serial.println("timer.isExpired()");
  }
    if (light4.isOn()) {
    light4.update();
  }
 /* if (timer.isExpired()) {
    Serial.println("timer.isExpired()");
  }*/

}
